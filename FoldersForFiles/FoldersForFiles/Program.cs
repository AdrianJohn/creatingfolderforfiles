﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FoldersForFiles {
    class Program {
        /// <summary>
        /// Checks every file in the execution folder and shifts every msg detected into its own folder.
        /// 
        /// originally Eg X:\BobsFiles\Walkthrough00.pdf -> X:\BobsFiles\Walkthrough00\Walkthrough00.pdf
        /// Now - "FA12345BobsFiles.msg" -> 12345/FA12345BobsFiles.msg
        /// 
        /// </summary>
            static void Main(string[] args)
            {
                string currentDir = Directory.GetCurrentDirectory();
                string[] files = Directory.GetFiles(currentDir, "*.JPG"); //

                Regex regex = new Regex(@"\(+.*\)+");

                foreach (string filePath in files)
                {
                    //string filename = filePath.Replace(currentDir + "\\", "");
                    string filename = Path.GetFileName(filePath);
                    Console.WriteLine("File: " + filename);

                    Match folderName = regex.Match(filename);
                    string newFolder = Path.Combine(currentDir, folderName.Value.Replace(" ", ""));

    

                if (!Directory.Exists(newFolder))
                    {
                        Directory.CreateDirectory(newFolder);
                    }
                    File.Move(filePath, Path.Combine(newFolder, filename));
                        Console.WriteLine("> moved to ." + newFolder.Replace(currentDir, ""));
                   
            }

            Console.WriteLine("Press Enter to quit");
            Console.ReadLine();
        }
    }
}
